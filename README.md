## Structure
The `styles` folder contains metadata for styles ID 1-157215  
JSON files contain unaltered API responses for https://userstyles.org/api/v1/styles/\<id>

## Missing styles (see [missing.json](https://gitlab.com/heterosexual/archives/userstyles.org/raw/master/missing.json))
The API returns a 500 error (probably because of long styles) on these 10 IDs:  
21009, 36776, 47871, 50814, 53683, 53979, 55793, 55891, 62434, 118049

Names, descriptions and authors can be recovered through [.user.js install links](https://userstyles.org/styles/userjs/21009/.user.js)  
Styles are still accessible at [/styles/\<id>.css](https://userstyles.org/styles/21009.css)

Other styles and comments cannot be recovered to my knowledge

## FAQ
> I get a 503 error when trying to load the styles folder from the web Gitlab UI

Either clone the repo or [access styles directly](https://gitlab.com/heterosexual/archives/userstyles.org/raw/master/styles/1234.json)

## Todo
- Script to generate .user.js
- Generate JSON files for recoverable missing styles

## Misc info
- [Snippets](https://gitlab.com/heterosexual/archives/userstyles.org/snippets)
